<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//login
Route::get('/', function () {
    return view('auth.login');
});
Route::get('login',['as'=>'login' ,function () {
    return view('auth.login');
}]);
Route::post('/userlogin',['as'=>'/userlogin','uses'=>'AuthController@login']);

Route::group(['middleware' => 'auth'], function()
{
    Route::get('/administrator/dashboard', ['as' => '/administrator/dashboard', 'uses' => 'AuthController@dashboard']);
    Route::get('/administrator/userlist', ['as' => '/administrator/userlist', 'uses' => 'AuthController@userlist']);
    Route::get('/administrator/submittedapplicantchoices', ['as' => '/administrator/submittedapplicantchoices', 'uses' => 'AuthController@submittedapplicantchoices']);
    Route::get('/administrator/confirmadmission', ['as' => '/administrator/confirmadmission', 'uses' => 'AuthController@confirmadmission']);
    Route::get('/administrator/rejectedadmission', ['as' => '/administrator/rejectedadmission', 'uses' => 'AuthController@rejectedadmission']);
    Route::get('/administrator/resubmittedapplication', ['as' => '/administrator/resubmittedapplication', 'uses' => 'AuthController@resubmittedapplication']);


});

//logout
Route::get('/logout', ['as'=>'/logout','uses'=>'AuthController@logout']);

