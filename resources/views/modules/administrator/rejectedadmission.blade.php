@extends('modules.layout.template')

@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Bordered Table</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            <th style="width: 10px">#</th>
                            {{--<th>Username</th>--}}
                            {{--<th>Token</th>--}}
                            <th>Istitution</th>
                            <th>F4indexno</th>
                            <th>Action</th>

                            </thead>
                            <tbody>
                            @php
                                $sno = 1;
                            @endphp
                            @foreach($rejectedadmission as $user)
                                <tr>
                                    <td>{{$sno}}</td>
                                    {{--<td>{{ $user->username }}</td>--}}
                                    {{--<td>{{ $user->token }}</td>--}}
                                    <td>{{ $user->institutionCode }}</td>
                                    <td>{{ $user->f4indexno }}</td>
                                    <td>{{ $user->reject_action }}</td>
                                </tr>
                                @php
                                    $sno++;
                                @endphp
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->

                <!-- /.box -->
            </div>
            <!-- /.col -->
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection