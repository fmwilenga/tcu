@extends('modules.layout.template')

@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Bordered Table</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            <th style="width: 10px">#</th>
                            {{--<th>Username</th>--}}
                            {{--<th>Token</th>--}}
                            <th>Istitution</th>
                            <th>F4indexno</th>
                            <th>f6indexno</th>
                            <th>Entry category</th>
                            <th>Selected programme</th>
                            <th>Admisson Status</th>
                            <th>Admitted Programme</th>
                            <th>Reason</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>otherf4index</th>
                            <th>otherf6index</th>
                            </thead>
                            <tbody>
                            @php
                                $sno = 1;
                            @endphp
                            @foreach($resubmittedapplication as $user)
                                <tr>
                                    <td>{{$sno}}</td>
                                    {{--<td>{{ $user->username }}</td>--}}
                                    {{--<td>{{ $user->token }}</td>--}}
                                    <td>{{ $user->institutionCode }}</td>
                                    <td>{{ $user->f4indexno }}</td>
                                    <td>{{ $user->f6indexno }}</td>
                                    <td>{{ $user->category }}</td>
                                    <td>{{ $user->selectedProgramme }}</td>
                                    <td>{{ $user->admissionStatus }}</td>
                                    <td>{{ $user->programmesAdmitted }}</td>
                                    <td>{{ $user->reason }}</td>
                                    <td>{{ $user->mobilenumber }}</td>
                                    <td>{{ $user->emailaddress }}</td>
                                    <td>{{ $user->otherf4indexno }}</td>
                                    <td>{{ $user->otherf6indexno }}</td>
                                </tr>
                                @php
                                    $sno++;
                                @endphp
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->

                <!-- /.box -->
            </div>
            <!-- /.col -->
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection