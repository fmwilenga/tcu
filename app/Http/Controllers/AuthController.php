<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\User;

class AuthController extends Controller
{
   	//login 
    public function login(Request $request)
    {

    	$this->validate($request,[
    		'username' => 'required',
    		'password'=>'required|min:8'
    	]);

    	$username=$request['username'];
    	$password=$request['password'];

    	if(Auth::attempt(['username'=>$username,'password'=>$password])){

    		$role = User::where('username',$username)->value('module_id');
                
    		return response()->json(['role' => $role],200);
    	}else{
    		$message = "Enter username and password correctly";

    		return response()->json(['message'=>$message],200);
    	}

    }

    public function dashboard()
    {
        return view('modules.administrator.dashboard');
    }


    public function logout(){
    	Auth::logout();

    	return redirect('/login');

    }

    public function userlist()
    {
        $userlist = DB::table('firstreques')
            ->get();
        return view('modules.administrator.dashboard', compact('userlist'));
    }

    public function submittedapplicantchoices()
    {
        $applicationChoices = DB::table('secondrequest')
            ->get();
        return view('modules.administrator.applicationChoices', compact('applicationChoices'));
    }

    public function confirmadmission()
    {
        $confirm_multiple_selection = DB::table('confirm_multiple_selection')
            ->get();
        return view('modules.administrator.confirm_multiple_selection', compact('confirm_multiple_selection'));
    }

    public function rejectedadmission()
    {
        $rejectedadmission = DB::table('rejectadmission')
            ->get();
        return view('modules.administrator.rejectedadmission', compact('rejectedadmission'));
    }

    public function resubmittedapplication()
    {
        $resubmittedapplication = DB::table('resubmitapplicantdetails')
            ->get();
        return view('modules.administrator.resubmittedapplication', compact('resubmittedapplication'));
    }



}
